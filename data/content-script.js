var new_codes = document.getElementById("files_bucket")
  .getElementsByClassName("blob-code-addition")

var regex = {
  "line comment khong dung": /^\s*#\S+/,
  "dang su dung tab": /\t/,
  "khong co khoang trang sau , hoac ;": /[,;]\S/,
  "co khoang trang truoc , hoac ;": /\s[,;]/,
  "dong > 80 ki tu": /.{81,}/,
  "su dung hash kieu cu": /:\w+\s+=>/,
  "khoang trang sau {, [ hoac (": /(([\{\(\[]\s)|(^\s[\}\)\]]$))/,
  "khoang trang truoc }, ] hoac )": /\S+\s+[\}\)\]]$/,
  "khoang trang cuoi dong": /^.+\s$/,
  "khoang trang phai la boi cua 2": /^(\s{2})*\s\S/,
  "thieu khoang trang truoc %> hoac -%>": /(([^-\s]%>)|(\S-%>))/,
  "thieu khoang trang sau <% hoac <%=": /((<%[^=\s])|(<%=\S))/,
  "dang su dung '": /^[^']*'[^']*'/,
  "dang su dung \"": /^[^"]*"[^"]*"/,
  "tu khoa khong hop le(trong ruby)": /byebug|binding\.pry/,
  "tu khoa khong hop le(trong javascript)": /console\.log/
}

var errors = {
  "default": [
    "dang su dung tab",
    "khong co khoang trang sau , hoac ;",
    "dong > 80 ki tu",
    "khoang trang sau {, [ hoac (",
    "khoang trang truoc }, ] hoac )",
    "khoang trang cuoi dong",
    "khoang trang phai la boi cua 2",
    "co khoang trang truoc , hoac ;"
  ],
  ".js": [
    "line comment khong dung",
    "dang su dung \"",
    "tu khoa khong hop le(trong javascript)"
  ],
  ".css": [],
  ".rb|.erb": [
    "line comment khong dung",
    "su dung hash kieu cu",
    "thieu khoang trang truoc %> hoac -%>",
    "thieu khoang trang sau <% hoac <%=",
    "dang su dung '",
    "tu khoa khong hop le(trong ruby)"
  ],
  ".yml": []
}

var results = {}

function scan_errors (new_code, errors_by_type, element) {
  console.log(errors_by_type)
  for(var index in errors_by_type) {
    console.log(index)
    if (regex[errors_by_type[index]].test(new_code)) {
      console.log(errors_by_type[index]);
      console.log(new_code);
      element.style.backgroundColor = "yellow";
      element.title = errors_by_type[index];
      if(typeof results[errors_by_type[index]] == "undefined") {
        results[errors_by_type[index]] = 1
      } else {
        results[errors_by_type[index]] += 1
      }
    }
  }
}

for(var i = 0; i < new_codes.length; i++){
  var new_code = new_codes[i].textContent.trim().substring(1, new_codes[i].length)
  var element = new_codes[i]
  var current_file = new_codes[i].parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByClassName("file-header")[0].dataset.path

  for (var type in errors) {
    if (type == "default") {
      scan_errors(new_code, errors[type], element)
    } else {
      for(var each_type in type.split("|")) {
        if(current_file.indexOf(each_type) >= 0) {
          scan_errors(new_code, errors[type], element)
        }
      }
    }
  }
}

if (Object.keys(results).length > 0) {
  var results_content = "Results\n";
  for (var error in results){
    results_content += results[error] + " loi " + error + "\n";
  }
  alert(results_content)
} else {
  alert("Code OK")
}
