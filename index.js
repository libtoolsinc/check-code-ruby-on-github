var buttons = require('sdk/ui/button/action');
var tabs = require('sdk/tabs');

tabs.on("pageshow", function (tab) {
  var patt = /https:\/\/github.com\/(\w+\/)+pull\/\d+.*/i
  console.log(tab.url)
  if(patt.test(tab.url)) {
    tab.attach({
      contentScriptFile: "./content-script.js"
    })
  }
})
